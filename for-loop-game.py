from random import randint

name = input("Hi! What is your name?")
min_month = 1
max_month = 12
min_year = 1920
max_year = 2022

for i in range(1,6):
    guessed_month = randint(min_month, max_month)
    guessed_year = randint(min_year, max_year)

    print("Guess ", i, ": ", name, "were you born in", guessed_month, "/", guessed_year, "?")
    user_input = input("Is my guess TOO HIGH, TOO LOW or CORRECT?")

    if (user_input == "TOO HIGH") and i < 6:
        print("Crap! Lemme try again")
        max_year = guessed_year
    elif (user_input == "TOO LOW") and i < 6:
        print("Crap! Lemme try again")
        min_year = guessed_year + 1
    elif (user_input == "CORRECT") and i < 6:
        print("I knew it!")
    elif (user_input != "TOO HIGH" or user_input != "TOO LOW" or user_input != "CORRECT"):
        print("Please enter a correct response.")
    else:
        exit()